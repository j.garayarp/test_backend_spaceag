-r ./base.txt

# Debugging
ipdb==0.11

# Tools
django-extensions==2.2.9

# Testing
pytest==6.2.5
pytest-sugar==0.9.4
pytest-django==4.4.0
factory-boy==3.2.0

# Code quality
flake8==3.6.0
