# Base
psycopg2==2.9.1 --no-binary psycopg2

# Django
django==3.2.7

# Environment
django-environ==0.7.0

# Django REST Framework
djangorestframework==3.12.4

