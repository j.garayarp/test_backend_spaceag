# Space AG - Test Back Django

## Instrucciones

## En local

### 1. Construir imagenes `docker-compose -f local.yml build`
### 2. Levantar proyecto `docker-compose -f local.yml up`
### 3. Acceder a los endpoints
* Crear HTTP POST desde localhost:8000/v1/field_workers/
* Listar HTTP GET desde localhost:8000/v1/field_workers/ (Paginado de 10 items)
* Actualizar parcialmente desde HTTP PATCH desde localhost:8000/v1/field_workers/{id}
* Borrar HTTP DELETE desde localhost:8000/v1/field_workers/{id}

## Correr tests
### 1. Ejecutar el siguiente comando `docker-compose -f local.yml run --rm django pytest`

## En producción

### 1. Construir imagenes `docker-compose -f production.yml build`
### 2. Realizar migraciones `docker-compose -f production.yml run --rm web python manage.py migrate`
### 3. Levantar proyecto `docker-compose -f production.yml up`
### 4. Acceder a los endpoints
* Crear HTTP POST desde localhost/v1/field_workers/
* Listar HTTP GET desde localhost/v1/field_workers/ (Paginado de 10 items)
* Actualizar parcialmente desde HTTP PATCH desde localhost/v1/field_workers/{id}
* Borrar HTTP DELETE desde localhost/v1/field_workers/{id}