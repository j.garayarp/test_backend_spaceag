"""Main URLs module."""

from django.urls import include, path

APP_VERSION = 'v1'

urlpatterns = [

    path('{}/'.format(APP_VERSION), include('spaceag.field_workers.urls')),

]
