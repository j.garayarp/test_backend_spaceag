"""Field Worker model."""

# Django
from django.db import models

# Constants
from spaceag.field_workers.constants import (
    OPTION_CHOICES,
    OPTION_4,
)


class FieldWorker(models.Model):
    """Field Worker model.
    
    """

    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    function = models.CharField(
        max_length=10,
        choices=OPTION_CHOICES,
        default=OPTION_4
    )
    created_at = models.DateTimeField(
        'created at',
        auto_now_add=True,
        help_text='Date time on which the object was created.'
    )