"""Field workers app."""

# Django
from django.apps import AppConfig


class FieldWorkersAppConfig(AppConfig):
    """Field workers app config."""
    default_auto_field = 'django.db.models.AutoField'
    name = 'spaceag.field_workers'
    verbose_name = 'Field Workers'