"""Constants variables for Field Workers"""

OPTION_1 = "harvest"
OPTION_2 = "pruning"
OPTION_3 = "scouting"
OPTION_4 = "other"
OPTION_CHOICES = (
    (OPTION_1, "Harvest"), 
    (OPTION_2, "Pruning"),
    (OPTION_3, "Scouting"),
    (OPTION_4, "Other"),
)
