"""Field workers tests."""

# Django REST Framework
from rest_framework import status
from rest_framework.test import APITestCase

# Model
from spaceag.field_workers.models import FieldWorker


class FieldWorkersAPITestCase(APITestCase):
    """Field worker API test case."""

    def setUp(self):
        """Test case setup."""
        self.field_worker = FieldWorker.objects.create(
            first_name='Testing',
            last_name='Testing',
        )

    def test_list_response_success(self):
        """Verify response succeed."""
        url = '/v1/field_workers/'
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create(self):
        """Verify if we can create a field worker."""
        url = '/v1/field_workers/'
        response = self.client.post(
            url,
            {
                'first_name': 'Testing',
                'last_name': 'Testing'
            },
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_partial_update(self):
        """Verify if we can update a field worker."""
        # Update the field worker
        url = '/v1/field_workers/{}/'.format(
            self.field_worker.id
        )
        response = self.client.patch(
            url,
            {
                'first_name': 'Testing 2'
            },
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete(self):
        """Verify if we can delete a field worker."""
        # Delete the field worker
        url = '/v1/field_workers/{}/'.format(
            self.field_worker.id
        )
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)