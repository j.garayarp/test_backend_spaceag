"""Field workers views."""

# Django REST Framework
from rest_framework import mixins, viewsets

# Serializers
from spaceag.field_workers.serializers import FieldWorkerModelSerializer

# Models
from spaceag.field_workers.models import FieldWorker


class FieldWorkerViewSet(mixins.CreateModelMixin,
                        mixins.ListModelMixin,
                        mixins.UpdateModelMixin,
                        mixins.DestroyModelMixin,
                        viewsets.GenericViewSet):
    """Field workers view set."""

    serializer_class = FieldWorkerModelSerializer
    queryset = FieldWorker.objects.all().order_by('first_name', 'last_name')
