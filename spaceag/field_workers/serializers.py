# Django REST Framework
from rest_framework import serializers

# Model
from spaceag.field_workers.models import FieldWorker


class FieldWorkerModelSerializer(serializers.ModelSerializer):
    """Field worker model serializer."""

    class Meta:
        """Meta class."""
        model = FieldWorker
        fields = '__all__'