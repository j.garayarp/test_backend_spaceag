# Django
from django.urls import include, path

# Django REST Framework
from rest_framework.routers import DefaultRouter

# Views
from spaceag.field_workers.views import FieldWorkerViewSet

router = DefaultRouter()
router.register(r'field_workers', FieldWorkerViewSet, basename='field_worker')

urlpatterns = [
    path('', include(router.urls))
]
